import hypermedia.net.*;


UDP udp;  // define the UDP object
//float [] Euler = new float [3]; 
//float [] Acc = new float [3];
float [] R = { 
  1, 0, 0, 0, 
  0, 1, 0, 0, 
  0, 0, 1, 0, 
  0, 0, 0, 1
};
PFont font;
final int VIEW_SIZE_X = 1024, VIEW_SIZE_Y = 720;
//float [] calib = { 0, 0, 0 };

void setup() 
{
  udp = new UDP( this, 6666 );
  udp.listen( true );
  size(VIEW_SIZE_X, VIEW_SIZE_Y, P3D);
  font = loadFont("CourierNew36.vlw");
}



void buildBoxShape() {
  //box(60, 10, 40);
  noStroke();
  beginShape(QUADS);

  //Z+ (to the drawing area)
  fill(#00ff00);
  vertex(-20, -30, 5);
  vertex(20, -30, 5);
  vertex(20, 30, 5);
  vertex(-20, 30, 5);

  //Z-
  fill(#0000ff);
  vertex(-20, -30, -5);
  vertex(20, -30, -5);
  vertex(20, 30, -5);
  vertex(-20, 30, -5);

  //X-
  fill(#ff0000);
  vertex(-20, -30, -5);
  vertex(-20, -30, 5);
  vertex(-20, 30, 5);
  vertex(-20, 30, -5);

  //X+
  fill(#ffff00);
  vertex(20, -30, -5);
  vertex(20, -30, 5);
  vertex(20, 30, 5);
  vertex(20, 30, -5);

  //Y-
  fill(#ff00ff);
  vertex(-20, -30, -5);
  vertex(20, -30, -5);
  vertex(20, -30, 5);
  vertex(-20, -30, 5);

  //Y+
  fill(#00ffff);
  vertex(-20, 30, -5);
  vertex(20, 30, -5);
  vertex(20, 30, 5);
  vertex(-20, 30, 5);

  endShape();
}


void drawCube() {  
  pushMatrix();
  translate(VIEW_SIZE_X/2, VIEW_SIZE_Y/2 + 50, 0);
  scale(5, 5, 5);
  /*
    float angleX, angleY, angleZ;
   angleY = 180 - Euler[0]; // fits range between 0 to 360
   angleX = 180 - Euler[2]; // fits range between 0 to 360)
   // borrow the sign value of accelY to determine the angle of rotation
   float t = (Euler[1] >= 0)? 180 + Euler[1] : abs(Euler[1]);
   if (Acc[2] >= 0) {
   angleZ = (Euler[1] >= 0)? (270 + (90 - Euler[1])): t;
   }
   else {
   angleZ = (Euler[1] >= 0)? t : (90 + (90 + Euler[1]));
   }
   
   
   rotateZ(-angleZ*3.14/180+calib[1]*3.14/180);
   rotateX(angleX*3.14/180+calib[2]*3.14/180);
   rotateY(angleY*3.14/180+calib[0]*3.14/180);
   */
  // Alternative method: use the rotationalMatrix from Android
  // apply with rotate() (same way as demo)
  applyMatrix(R[0], R[1], R[2], R[3], 
  R[4], R[5], R[6], R[7], 
  R[8], R[9], R[10], R[11], 
  R[12], R[13], R[14], R[15]
    );

  buildBoxShape();

  popMatrix();
}


void draw() {
  background(#000000);
  fill(#ffffff);


  textFont(font, 23);
  textAlign(LEFT, TOP);
  //text("Accerlometer:\n" + Acc[0] + "\n" + Acc[1] + "\n" + Acc[2] , 20, 20);
  //text("Euler Angles:\nYaw (psi)  : " + Euler[0] + "\nPitch (theta): " + Euler[1] + "\nRoll (phi)  : " + Euler[2], 200, 20);

  drawCube();
}

void mousePressed() {
  /*
float angleY = 180 - Euler[0]; // fits range between 0 to 360)  
   calib[0]=angleY;
   float angleZ = 0;
   // borrow the sign value of accelY to determine the angle of rotation
   float t = (Euler[1] >= 0)? 180 + Euler[1] : abs(Euler[1]);
   if (Acc[2] >= 0) {
   angleZ = (Euler[1] >= 0)? (270 + (90 - Euler[1])): t;
   }
   else {
   angleZ = (Euler[1] >= 0)? t : (90 + (90 + Euler[1]));
   }
   calib[1]=angleZ;
   float angleX = 180 - Euler[2]; // fits range between 0 to 360)  
   calib[2]=angleX;
   */
  /*R = { 1, 0, 0, 0,
   0, 1, 0, 0,
   0, 0, 1, 0,
   0, 0, 0, 1 }; */
}

void receive( byte[] data, String ip, int port ) {  // <-- extended handler


  // get the "real" message =
  // forget the ";\n" at the end <-- !!! only for a communication with Pd !!!
  data = subset(data, 0, data.length);
  String message = new String( data );
  String[] list = split(message, ',');
  /*
  Acc[0]=Float.parseFloat(list[1]);
   Acc[1]=Float.parseFloat(list[2]);
   Acc[2]=Float.parseFloat(list[3]);
   Euler[0]=Float.parseFloat(list[4]);
   Euler[1]=Float.parseFloat(list[5]);
   Euler[2]=Float.parseFloat(list[6]);
   */
  R = new float[16]; // 4x4 rotational matrix, elements sequenced by rows
  for (int i = 0; i < 16; i++) {
    R[i] = Float.parseFloat(list[i+1]);
  }

  // print the result
  println( "receive: \""+message+"\" from "+ip+" on port "+port );
}

